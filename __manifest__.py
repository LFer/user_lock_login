# -*- coding: utf-8 -*-
{
    'name': "User Lock Login",

    'summary': """
        Lock user's log in""",

    'description': """
        Lock user's log in based on employee Workable Days/Hours
    """,

    'author': "Proyecta",
    'website': "http://odoo.proyectasoft.com/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Login',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web', 'hr'],

    # always loaded
    'data': [
        'views/odoo_login_view.xml',
        'views/hr_employee_view.xml',
        'data/mail_template_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}