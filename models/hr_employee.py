# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    can_login_whenever = fields.Boolean(string='User Can Log In Whenever')

    def get_trip_correction_email_to(self):
        users_to_notify = self.env['hr.employee'].search([('revisa_viajes', '=', True)])
        users_to_send = ''
        if len(users_to_notify) > 1:
            lastindex = len(users_to_notify)
            _iter = 0
            for user in users_to_notify:
                _iter += 1
                if lastindex != _iter:
                    users_to_send += user.user_id.login + ';'
                if lastindex == _iter:
                    users_to_send += user.user_id.login
        else:
            users_to_send = users_to_notify.user_id.login
        users_to_send = 'lferreira@proyectasoft.com'
        return users_to_send

    def get_trip_correction_response_name(self):
        users_to_notify = self.env['hr.employee'].search([('revisa_viajes', '=', True)])
        users_to_send = ''
        if len(users_to_notify) > 1:
            lastindex = len(users_to_notify)
            _iter = 0
            for user in users_to_notify:
                _iter += 1
                if lastindex != _iter:
                    users_to_send += user.user_id.name + ','
                if lastindex == _iter:
                    users_to_send += user.user_id.name

        else:
            users_to_send = users_to_notify.user_id.name

        return users_to_send

    def grant_system_access(self):
        for rec in self:
            rec.can_login_whenever = True
            rendering_context = dict(self._context)

            rendering_context.update({
                'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url',default='http://localhost:8069'),
            })

            mail_template = self.env.ref('user_lock_login.employee_login_permission_granted')
            ctx_template = mail_template.with_context(rendering_context)
            mail_with_ctx = ctx_template.sudo().send_mail(self.id)
            mail1_to_send = self.env['mail.mail'].browse(mail_with_ctx)
            mail1_to_send.sudo().send()
        return True

    def deny_system_access(self):
        for rec in self:
            rec.can_login_whenever = False
            rendering_context = dict(self._context)

            rendering_context.update({
                'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url',default='http://localhost:8069'),
            })

            mail_template = self.env.ref('user_lock_login.employee_login_permission_denied')
            ctx_template = mail_template.with_context(rendering_context)
            mail_with_ctx = ctx_template.sudo().send_mail(self.id)
            mail1_to_send = self.env['mail.mail'].browse(mail_with_ctx)
            mail1_to_send.sudo().send()
        return True