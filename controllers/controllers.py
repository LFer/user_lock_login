from datetime import datetime
from datetime import timedelta
import odoo
from odoo import http
from odoo.addons.resource.models.resource import float_to_time
from odoo.addons.web.controllers.main import Home
from odoo.http import request
from odoo.tools.translate import _


def _check_if_can_login():
    if request.params['login']:
        user = request.env['res.users'].sudo().search([('login', '=', request.params['login'])])
        employee = user.employee_ids[0] if user.employee_ids else False
        if not employee:
            return False, 'No employee set for the login'
        if employee.can_login_whenever:
            return True, ''
        calendar = employee.resource_calendar_id
        today = datetime.today() - timedelta(hours=3)
        dayofweek = today.today().weekday()
        in_working_hours = True
        calendar_lines = calendar.attendance_ids.filtered(lambda x: x.dayofweek == str(dayofweek))
        msg = 'No puede ingresar al sistema fuera de su horario laboral'
        for line in calendar_lines:
            check_in = datetime.time(datetime.now() - timedelta(hours=3))
            hour_from = float_to_time(line.hour_from)
            hour_to = float_to_time(line.hour_to)
            if not (hour_from < check_in < hour_to):
                msg += _(' (%s - %s)' % (hour_from.strftime('%H:%M'), hour_to.strftime('%H:%M')))
                in_working_hours = False
            else:
                in_working_hours = True
        if not in_working_hours:
            return False, msg
        else:
            return True, msg


def _send_mail_request_for_login():
    env = request.env
    rendering_context = dict(env.context)
    view_to_show = env.ref('hr.view_employee_tree')
    user = env['res.users'].sudo().search([('login', '=', request.params['login'])])
    rendering_context.update({
        'action_id_to_show': env['ir.actions.act_window'].search([('view_id', '=', view_to_show.id)], limit=1).id,
        'dbname': env.cr.dbname,
        'base_url': env['ir.config_parameter'].sudo().get_param('web.base.url', default='http://localhost:8069'),
        'user': user.name,
        'model': 'hr.employee',
        'reason': request.params['feedback'],
        'record_id': user.employee_ids[0].id if user.employee_ids else False,
    })

    mail_template = env.ref('user_lock_login.employee_login_ask_permission')
    ctx_template = mail_template.with_context(rendering_context)
    mail_with_ctx = ctx_template.sudo().send_mail(ctx_template.id)
    mail1_to_send = env['mail.mail'].browse(mail_with_ctx)
    mail1_to_send.sudo().send()


class Extension_Home(Home):

    @http.route('/web/login', type='http', auth="public")
    def web_login(self, redirect=None, **kw):
        request.params['login_success'] = False
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)

        if not request.uid:
            request.uid = odoo.SUPERUSER_ID

        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None
        if request.httprequest.method == 'POST':
            old_uid = request.uid
            if request.params['login']:
                ok, msg = _check_if_can_login()
                if not ok:
                    if request.params.get('feedback', False):
                        _send_mail_request_for_login()
                        request.uid = old_uid
                        values['error'] = 'Correo enviado espere'
                    else:
                        request.uid = old_uid
                        values['error'] = msg
                        values['show_request_for_permission'] = True

                else:
                    uid = False
                    try:
                        uid = request.session.authenticate(request.session.db, request.params['login'],
                                                           request.params['password'])
                    except odoo.exceptions.AccessDenied as e:
                        request.uid = old_uid
                        if e.args == odoo.exceptions.AccessDenied().args:
                            values['error'] = _("Wrong login/password")
                        else:
                            values['error'] = e.args[0]

                    if uid is not False:
                        request.params['login_success'] = True
                        if not redirect:
                            redirect = '/web'
                        return http.redirect_with_hash(redirect)
                    request.uid = old_uid
                    values['error'] = _("Wrong login/password")
        return request.render('web.login', values)
