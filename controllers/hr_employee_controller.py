import werkzeug.utils
from odoo import http
from odoo.http import request

class HrEmployee(http.Controller):
    @http.route('/approve', type='http', auth="public")
    def action_approve_access(self, db, token, action, record_id, model):
        env = request.env
        record = env[model].browse(int(record_id))
        record.sudo().grant_system_access()
        return werkzeug.utils.redirect('/web/login', 303)

    @http.route('/deny', type='http', auth="public")
    def action_deny_access(self, db, token, action, record_id, model):
        env = request.env
        record = env[model].browse(int(record_id))
        record.sudo().deny_system_access()
        return werkzeug.utils.redirect('/web/login', 303)